package com.fan.gupaolearn.pattern.singleton.register;

import java.util.HashMap;
import java.util.Map;

public class RegisterSingleton {

    private RegisterSingleton(){}

    private static Map<String, Object> ioc = new HashMap<>();

    public static Object getBean(String calss) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        synchronized (ioc) {
            Object o = null;
            if (!ioc.containsKey(calss)) {
                o = Class.forName(calss).newInstance();
                ioc.put(calss, o);
                return o;
            }else{
                return ioc.get(calss);
            }
        }
    }
}
