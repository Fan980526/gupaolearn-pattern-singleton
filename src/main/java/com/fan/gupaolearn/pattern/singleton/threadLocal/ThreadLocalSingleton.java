package com.fan.gupaolearn.pattern.singleton.threadLocal;

public class ThreadLocalSingleton {

    // 单个线程单例

    private static final ThreadLocal<ThreadLocalSingleton> THREAD_LOCAL =
            new ThreadLocal<ThreadLocalSingleton>() {
                @Override
                protected ThreadLocalSingleton initialValue() {
                    return new ThreadLocalSingleton();
                }
            };

    private ThreadLocalSingleton() {
    }

    public static ThreadLocalSingleton getInstance() {
        return THREAD_LOCAL.get();
    }
}
