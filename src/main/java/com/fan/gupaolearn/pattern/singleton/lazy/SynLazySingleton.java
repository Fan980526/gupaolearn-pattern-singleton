package com.fan.gupaolearn.pattern.singleton.lazy;

public class SynLazySingleton {

    private SynLazySingleton() {
    }

    private static SynLazySingleton lazySingleton = null;

    // 双重锁，改善了锁带来的CPU压力
    public static SynLazySingleton getLazySingleton() {
        if (lazySingleton == null) {
            synchronized (SynLazySingleton.class) {
                if (lazySingleton == null) {
                    lazySingleton = new SynLazySingleton();
                }
            }
        }
        return lazySingleton;
    }
}
