package com.fan.gupaolearn.pattern.singleton.register;

public class Test {

    public static void main(String[] args) {

        Thread thread1 = new Thread(new ExectorThread());
        Thread thread2 = new Thread(new ExectorThread());
        thread1.start();
        thread2.start();

    }
}
