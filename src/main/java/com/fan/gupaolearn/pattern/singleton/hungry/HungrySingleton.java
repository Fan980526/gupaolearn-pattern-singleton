package com.fan.gupaolearn.pattern.singleton.hungry;

public class HungrySingleton {

    private static final HungrySingleton hungrySingleton = new HungrySingleton();

    private HungrySingleton() {

    }

    public static HungrySingleton getHungrySingleton(){
        return hungrySingleton;
    }
}
