package com.fan.gupaolearn.pattern.singleton.register;

public enum EnumSingleton {

    FAN;

    private static EnumSingleton getFan(){
        return FAN;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
