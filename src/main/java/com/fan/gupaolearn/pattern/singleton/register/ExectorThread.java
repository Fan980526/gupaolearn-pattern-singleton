package com.fan.gupaolearn.pattern.singleton.register;

import com.fan.gupaolearn.pattern.singleton.App;

public class ExectorThread implements Runnable {
    @Override
    public void run() {
        try {
            App app = (App) RegisterSingleton.getBean("com.fan.gupaolearn.pattern.singleton.App");
            System.out.println(Thread.currentThread().getName() + " , " + app);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }
}
