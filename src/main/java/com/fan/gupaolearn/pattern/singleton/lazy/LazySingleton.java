package com.fan.gupaolearn.pattern.singleton.lazy;

public class LazySingleton {

    private LazySingleton(){}

    private static LazySingleton lazySingleton = null;

    // synchronized 为了避免线程不安全问题会破坏单例
    public synchronized static LazySingleton getLazySingleton(){
        if (lazySingleton == null){
            lazySingleton = new LazySingleton();
        }
        return lazySingleton;
    }
}
