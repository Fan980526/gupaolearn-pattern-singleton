package com.fan.gupaolearn.pattern.singleton.innerClass;

public class InnerClassSingleton {

    private InnerClassSingleton(){
        if (MySingleton.innerClassSingleton != null) {
            new Thread("不允许非法访问");
        }
    }

    //每一个关键字都不是多余的，static 是为了使单例的空间共享，保证这个方法不会被重写、重载
    public static final InnerClassSingleton getInnerClassSingleton(){
        // 这里会先加载静态内部类
        return MySingleton.innerClassSingleton;
    }

    // 静态内部类 在调用时才会被加载
    private static class MySingleton{
        private static final InnerClassSingleton innerClassSingleton = new InnerClassSingleton();
    }
}
