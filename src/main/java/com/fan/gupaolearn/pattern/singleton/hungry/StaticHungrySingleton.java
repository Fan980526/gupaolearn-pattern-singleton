package com.fan.gupaolearn.pattern.singleton.hungry;

public class StaticHungrySingleton {

    static{
        hungrySingleton = new StaticHungrySingleton();
    }

    private static final StaticHungrySingleton hungrySingleton;

    private StaticHungrySingleton() {

    }

    public static StaticHungrySingleton getHungrySingleton(){
        return hungrySingleton;
    }
}
